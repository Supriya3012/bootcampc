
/*
 take rows from the user
 10
 I H
 7 6 5
 D C B A
*/

#include<stdio.h>
void main(){

	int r;

	printf("Enter rows\n");
	scanf("%d",&r);
	
	int sum=0;

	for(int k=1;k<=r;k++){

		sum =sum+k;
	}

	for(int i=1;i<=r;i++){

		for(int j=1;j<=i;j++){

			if(i%2==1){

				printf("%d ",sum);
				sum--;
			}else{
				printf("%c ",64+sum);
				sum--;
			}
		}
		printf("\n");
	}
}
	
