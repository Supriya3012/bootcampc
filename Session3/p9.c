
/*
 Take rows from the user
 R=3
 3 
 6 9
 12 15 18

 r=4

 4
 8 12
16 20 24
28 32 36 40
*/

#include<stdio.h>
void main(){

	int r;

	printf("Enter rows\n");
	scanf("%d",&r);

	int x=r;

	for(int i=1;i<=r;i++){

		for(int j=1;j<=i;j++){

			printf("%d ",x);
			x=x+r;
		}
		printf("\n");
	}
}
