
/*
 take no of rows from the user rows =3

 A b C
 d E f
 G h I


*/
#include<stdio.h>
void main(){

	int r;

	printf("enter rows\n");
	scanf("%d",&r);

	int x = 96+r-(r-1);
	int y = 64+r-(r-1);

	for(int i=1;i<=r;i++){

		for(int j=1;j<=r;j++){

			if(i%2==0 || j%2 ==0){

				printf("%c ",x);
			
			}else{
				printf("%c ",y);
			
			}
			x++;
			y++;
		}
		printf("\n");
	}
}
