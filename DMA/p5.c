

//realloc : program1


#include<stdio.h>
#include<stdlib.h>

void main(){

	int *ptr = (int *) calloc (5,sizeof(int)); 		//calloc

	for(int i=0;i<5;i++){

		*(ptr+i)=(10+i);

	}
	printf("calloc\n"); 
	for(int i=0;i<5;i++){

		printf("%d \n",*(ptr+i));
	}

	int *ptr2 = (int *) realloc (ptr ,8);		//realloc 	//increase size of array 5 to 8

	for(int i=0;i<8;i++){

		*(ptr+i)=10+i;

	}

	printf("realloc\n");

	for(int i=0;i<8;i++){

		printf(" %d \n",*(ptr+i));
	}
}
