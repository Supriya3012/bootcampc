
/*
 WAP to print factorial of given number by user
 i/p:5
 o/p: factorial of 5 is 120
*/
#include<stdio.h>

void main(){

	int num1,fact=1;

	printf("Enter number\n");
	scanf("%d",&num1);

	for(int i=1;i<=num1;i++){

		fact=fact * i;
	}
	printf("factorial of %d is %d \n",num1,fact);
}
