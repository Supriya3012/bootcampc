
/*
 Take rows from the user

 4 3 2 1
 4 3 2
 4 3
 4

 */

#include<stdio.h>
void main(){

	int r;

	printf("Enter rows\n");
	scanf("%d",&r);

	for(int i=1;i<=r;i++){

		int x=r;

		for(int j=r;j>=i;j--){

			printf("%d ",x);
			x--;
		}
		printf("\n");
	}
}
