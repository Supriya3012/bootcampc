
/*
 Print the factorial of each number between given range 
start:1
End : 5
o/p: Fact of 1 is 1
     fact of 2 is 2
     fact of 3 is 6
     fact of 4 is 24
     fact of 5 is 120
     */

#include<stdio.h>
void main(){

	int num1, num2,fact =1;

	printf("Enter num1 ,num2\n");
	scanf("%d%d",&num1,&num2);

	for(int i=num1;i<=num2;i++){

		fact = fact * i;

		printf("factorial of %d is %d \n",i,fact);
	}
}
