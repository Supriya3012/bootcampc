

/*
 Take rows from the user

 r=4

 D C B A
 C B A
 B A
 A
 */

#include<stdio.h>
void main(){

	int r;

	printf("Enter rows\n");
	scanf("%d",&r);

	int ch = 64+r;

	for(int i=1;i<=r;i++){

		for(int j=r;j>=i;j--){

			printf("%c ",ch);
			ch--;
		}

		ch= (64+r)-i;
		printf("\n");
	}
}

