

/*
 Take rows from the user
 r=4
 1 2 3 4
 5 6 7
 8 9
 10
 */

#include<stdio.h>
void main(){

	int r;

	printf("Enter rows\n");
	scanf("%d",&r);

	int x=r-(r-1);

	for(int i=1;i<=r;i++){

		for(int j=r;j>=i;j--){

			printf("%d ",x);
			x++;
		}
		printf("\n");
	}
}
