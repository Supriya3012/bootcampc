
/* Take the no of rows from the user
1 2 3 4
  1 2 3
    1 2
      1

  */

#include<stdio.h>
void main(){

	int r;

	printf("Enter rows\n");
	scanf("%d",&r);

	for(int i=1;i<=r;i++){

		for(int sp=1;sp<i;sp++){

			printf("  ");
		}
		int x = r-(r-1);

			for(int j=r;j>=i;j--){

				printf("%d ",x);
				x++;
			
			}
			printf("\n");
	}
}
