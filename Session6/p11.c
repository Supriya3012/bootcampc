
/*
 Take no of rows from the user
 1 3 5 7 9
   9 7 5 3
     3 5 7
       7 5
         5
	 */

#include<stdio.h> 
void main(){

	int r;

	printf("Enter rows\n");
	scanf("%d",&r);

	int x=r-(r-1);

	for(int i=1;i<=r;i++){

		for(int sp=1;sp<i;sp++){

			printf("  ");
		}

			for(int j=r;j>=i;j--){
    			
				if(i%2==1){

					printf("%d ",x);
					x=x+2;
				}else{
					printf("%d ",x);
					x=x-2;
				}
			}
			if(i%2==1){
				x=x-2;
			}else{
				x=x+2;
			}
			printf("\n");
	}
}



