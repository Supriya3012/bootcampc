
/*
 Take no of rows form the user
 D D D D
   c c c
     B B
       a
       */

#include<stdio.h>
void main(){

	int r;

	printf("Enter rows\n");
	scanf("%d",&r);

	int x=r+64;

	for(int i=1;i<=r;i++){

		for(int sp=1;sp<i;sp++){

			printf("  ");
		}

			for(int j=r;j>=i;j--){

				if(i%2==1){

					printf("%c ",x);
				}else{

					printf("%c ",x+32);
				}
			}
			x--;
			printf("\n");
	}
}
