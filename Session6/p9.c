
/* Take no of rows from the user
 
   100 9 64 7
      36 5 16
         3 4
	   1

	  */

#include<stdio.h>
void main(){

	int r;

	printf("Enter rows\n");
	scanf("%d",&r);

	int sum=0;
	for(int i=1;i<=r;i++){

		sum = sum+i;
	}

		for(int i=1;i<=r;i++){
		
			for(int sp=1;sp<i;sp++){

				printf("  ");
			}
				for(int j=r;j>=i;j--){

					if(sum%2==0){
						int sq=1;
						sq=sum*sum;
						printf("%d ",sq);
						sum--;
					}else{

						printf("%d ",sum);
						sum--;
					}
				}
				printf("\n");
		}
}
