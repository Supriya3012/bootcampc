

//WAP to print the number whose factorial is even . take range from the user
//i/p: start:1
//	End: 5
//o/p:2 3 4 5

#include<stdio.h>
void main(){

	int num1,num2,fact=1;

	printf("Enter number1 and number 2\n");
	scanf("%d%d",&num1,&num2);

	printf("Even factorial numbers are :");

	for(int i=num1;i<=num2;i++){

		fact =fact*i;

			if(fact %2==0){

				printf("%d ",i);
			}
	}
}
