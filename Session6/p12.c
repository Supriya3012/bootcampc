
/*
 A b C d
   e G i
     K n
       q

       */

#include<stdio.h>
void main(){

	int r;

	printf("Enter rows\n");
	scanf("%d",&r);

	int x= r-(r-1)+64;

	for(int i=1;i<=r;i++){

		for(int sp=1;sp<i;sp++){

			printf("  ");
		}

		for(int j=r;j>=i;j--){

			if(i%2==1){
				if(j%2==0){

          			printf("%c ",x);
					x=x+i;
				}else{
					printf("%c ",x+32);
					x=x+i;
				}
			}else{
				if(j%2==0){
					printf("%c ",x+32);
					x=x+i;
				}else{
					printf("%c ",x);
					x=x+i;
				}
			}
		}
		printf("\n");
	}
}
