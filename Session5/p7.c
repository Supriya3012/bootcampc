
/*
 Take no rows from the user
 	
         A
       b a
     C E G
   d c b a
 E G I K M
 */

#include<stdio.h>
void main(){

	int r;

	printf("Enter rows\n");
	scanf("%d",&r);

	int x=r-(r-1)+64;

	for(int i=1;i<=r;i++){

		for(int sp=r;sp>i;sp--){

			printf("  ");
		}

			for(int j=1;j<=i;j++){

				if(i%2==1){

					printf("%c ",x);
					x=x+2;
				}else{

					printf("%c ",x+32);
					x--;
				}
			}
			x=r-(r-1)+64;
			x=x+i;
			printf("\n");
	}
}
