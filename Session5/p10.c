
/*
 Take no of rows from the user

       4
     3 6
   2 4 6
 1 2 3 4

 */

#include<stdio.h>
void main(){

	int r;

	printf("Enter rows\n");
	scanf("%d",&r);

	int x=r;
	int sq=1;

	for(int i=1;i<=r;i++){

		for(int sp=r;sp>i;sp--){

			printf("  ");

		}

			for(int j=1;j<=i;j++){

				sq=x*j;
				printf("%d ",sq);
				
			}

			x=r-i;
			printf("\n");
	}
}
