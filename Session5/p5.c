
/*
 Take rows from the user

	  1
       5  9
    13 17 21
 25 29 33 37
 */

#include<stdio.h>
void main(){

	int r;

	printf("Enter rows\n");
	scanf("%d",&r);

	int x=r-(r-1);

	for(int i=1;i<=r;i++){

		for(int sp=r;sp>i;sp--){

			printf("   ");
		}

			for(int j=1;j<=i;j++){

				printf("%d ",x);
				x=x+r;
			}
			printf("\n");
	}
}
