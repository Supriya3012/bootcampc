
/*
 Take rows from the user

         5
       5 6
     5 4 3
   5 6 7 8
 5 4 3 2 1

 */

#include<stdio.h>

void main(){

	int r;

	printf("Enter rows\n");
	scanf("%d",&r);

	for(int i=1;i<=r;i++){

		int x=r;

		for(int sp=r;sp>i;sp--){

			printf("  ");
		}

			for(int j=1;j<=i;j++){

				if(i%2==1){

					printf("%d ",x);
					x--;

				}else{

					printf("%d ",x);
					x++;

				}
			}
			printf("\n");
	}
}
