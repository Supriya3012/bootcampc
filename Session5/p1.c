

/*
 take rows from the user
        1
      1 2
    1 2 3
  1 2 3 4

  */

#include<stdio.h>
void main(){

	int r;

	printf("Enter rows\n");
	scanf("%d",&r);

	for(int i=1;i<=r;i++){

		for(int sp =r;sp>i;sp--){

			printf("  ");
		}

			for(int j=1;j<=i;j++){

				printf("%d ", j);
			
			}
			printf("\n");
	}
}

