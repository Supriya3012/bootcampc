/*
 Take the rows from the user

       1
     A b
   1 2 3
 A b C d

 */

#include<stdio.h>

void main(){

	int r;

	printf("Enter rows\n");
	scanf("%d",&r);

	
	for(int i=1;i<=r;i++){

		int x= r-(r-1);
		int y=r-(r-1)+64;

		for(int sp =r;sp>i;sp--){

			printf("  ");

		}

			for(int j=1;j<=i;j++){

				if(i%2==1){

					printf("%d ",x);
					x++;

				}else if(j%2==1){

					printf("%c ",y);
					y++;
				}else{
					printf("%c ",y+32);
					y++;
				}
			}
			printf("\n");
	}
}
