
/*
 WAP to printf the addition of factorial of given no. from user
 i/p:num1:4     //24
     num2:5     //120
     o/p:addition of factorial of 4&5 is 144

   */

#include<stdio.h>
void main(){

	int num1,num2;

	int fact1=1;
	int fact2=1;

	printf("Enter num1&num2\n");
	scanf("%d%d",&num1,&num2);

	for(int i=1;i<=num1;i++){

		fact1=fact1*i;
	}

		for(int i=1;i<=num2;i++){

			fact2=fact2*i;
		}

	      int fact =fact1+fact2;

		printf("addition of fact of %d %d is %d ",num1,num2,fact);
}
