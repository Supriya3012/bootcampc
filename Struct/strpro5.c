
//Structure:program5
//datatype:pointer

#include<stdio.h>

struct Demo {

	float f1;						//8 bytes
	char *ch1;		//pointer : 8 bytes :max size  //8 bytes
	int x;							//8 bytes :4 for int x 4 for int y
	int y;
};

void main(){

	printf("%ld \n",sizeof(struct Demo));		//8+8+8 =24
}

