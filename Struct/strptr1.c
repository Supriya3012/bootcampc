
//Pointer to structure(Structure pointer)
//Program1:

#include<stdio.h>

struct Movie{

	char mName[20];
	int count;
	float price;

}obj1 = {"Tumbbad",5,1000.00};

void main(){

	struct Movie *sptr = &obj1;  // structure pointer

	printf("%s\n",obj1.mName);
	printf("%d \n",obj1.count);		//by using object
	printf("%f\n",obj1.price);

	printf("%s\n",(*sptr).mName);
	printf("%d\n",(*sptr).count);		//by using pointer
	printf("%f\n",sptr -> price);
}
