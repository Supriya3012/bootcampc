
//Nested Structure

#include<stdio.h>


struct Movie{

	char mName[10];
	
	struct MovieInfo{

		char actor[20];
		float Imdb;

	}obj1;

};

void main(){

	struct Movie obj2={"RHTDM",{"R.madhavan",8.5}};

	printf("%s\n",obj2.mName);
	printf("%s\n",obj2.obj1.actor);
	printf("%f\n",obj2.obj1.Imdb);
}

