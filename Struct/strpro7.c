

//struct :program7
//pragma pack(1) : memory allignment as per datatype

#include<stdio.h>
#pragma pack(1)

struct Demo {			//with pragma		without pragma

	char ch1;		// 1 bytes		//8  : 1 for char 4 for int 3:waste
	int x;			// 4 			
	float y;		//4			//8   :4 for float 4 :waste
	double arr[5];  	//5*8=40 		//40

};

void main(){

	printf("%ld \n",sizeof(struct Demo));	//1+4+4+40=49(with pragma pack)
						//8+8+40=56 (without pragma pack)
}
