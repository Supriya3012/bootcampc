

//Structure :Program 4

#include<stdio.h>

struct Demo {

	int jerNo;		//8 bytes : 4 for int 1 for char
	char ch1;
	float avg;		//8 bytes : 4 for float
	double grades;		//max datatype:8 //8 bytes : 8 for double

};

void main(){

	struct Demo obj;

	printf("%ld\n",sizeof(obj));			//8+8+8 =24
	printf("%ld \n",sizeof(struct Demo));		//24
}

