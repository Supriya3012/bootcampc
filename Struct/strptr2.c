
//Array of Structure

#include<stdio.h>

struct player{

	char pname[10];
	int jerNo;                 //sizeof strcut 20 bytes 4+4+12(char array)
	float rev;

};

void main(){

	struct player obj1 = {"Virat",18,980.50};
	struct player obj2 = {"Messi",10,4580.50};
	struct player obj3 = {"James",6,9500.50};

	struct player arr[3]={obj1,obj2,obj3};		//array of structure

	for(int i=0;i<3;i++){

		printf("%s\n",arr[i].pname);
		printf("%d\n",arr[i].jerNo);
		printf("%f\n",arr[i].rev);
	}
}

