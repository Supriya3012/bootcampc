

//Nested structure
//Type 2

#include<stdio.h>

struct MovieInfo{

	char actor[10];
	float Imdb;

};

struct Movie{

	char mName[20];

	struct MovieInfo obj1;

};

void main(){

	struct Movie obj2 = {"Tumbbad",{"sohanshah",8.8}};	//nested structure: data assignment

	printf("%s\n",obj2.mName);
	printf("%s\n",obj2.obj1.actor);
	printf("%f\n",obj2.obj1.Imdb);
}

