
//Nested Structure
//Type -1

#include<stdio.h>
#include<string.h>

struct MovieInfo{

	char actor[10];
	float Imdb;
};

struct Movie {

	char mName[20];

	struct MovieInfo obj1;		//structure inside structure: (Nested Structure)
	
};

void main(){

	struct Movie obj2;

	strcpy(obj2.mName,"Kantaara");
	strcpy(obj2.obj1.actor,"Rishabh");
	obj2.obj1.Imdb=9.7;

	printf("%s\n",obj2.mName);
	printf("%s\n",obj2.obj1.actor);
	printf("%f\n",obj2.obj1.Imdb);
}
