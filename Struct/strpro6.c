
//Structure :program6
//datatype :array[]


#include<stdio.h>
 

struct Demo {

	char ch1;		//max datatype:int and float :4 bytes //4 bytes for char1
	int x;								//4 bytes
	float y;							//4 bytes
	char arr[10];				//4 bytes for 10 elements =12 box
	
	/*
	 char ch1;					//8 bytes :1 for ch1 ,4 for int
	 int x;
	 float y;					//8 bytes : 4 for float
	 double arr[5];                 //max datatype :8 bytes :8*5=40 bytes      //sizeof(struct Demo)=8+8+40=56 

	 */

};

void main(){

	printf("%ld \n",sizeof(struct Demo)); 		//4+4+4+12=24
}

