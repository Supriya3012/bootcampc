  
//array Initialization


#include<stdio.h>
void main(){

	int arr[5]={10,20,30,40,50}; //array initialization: initializer list

	printf("%d\n",arr[0]);  //%d = data //10

	printf("%d\n",arr);  //warning : arr is also pointer .it gives address.
	printf("%p\n",arr);   // address of 1st element
	printf("%p\n",&arr);  // address of whole array
 
}
